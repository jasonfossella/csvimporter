// resources/assets/js/components/App.js

import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, PageHeader, Header, Table } from 'react-bootstrap'

class App extends Component {
    constructor (props, context) {
        super(props, context);

        this.uploadFile = this.uploadFile.bind(this);

        this.state = {
            error: '',
            rows: []
        };
    }

    uploadFile(e){
        const data = new FormData();
        data.append('file', e.target.files[0]);

        fetch('/api/upload', {
            method: "POST",
            body: data
        })
            .catch(error => console.error('Error:', error))
            .then((response) => {
                if (response.ok) {
                    response.json().then((data) =>
                    {
                        this.setState({
                            error: '',
                            rows: data.transactions
                        })
                    });
                } else {
                    this.setState({
                        error: 'Error: ' + response.statusText,
                        rows: []
                    });
                }
            });
    }

    render () {
        return (
            <Grid>
                <Row>
                    <Col xs={12}>
                        <PageHeader>CSV Importer</PageHeader>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <h2>{this.state.error}</h2>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <form>
                            <FormGroup>
                                <ControlLabel>File to import </ControlLabel>
                                <FormControl
                                    id="formControlsFile"
                                    type="file"
                                    onChange={this.uploadFile}
                                />
                            </FormGroup>
                        </form>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12}>
                        <Table>
                            <thead>
                            <tr>
                                <th>Customer Number</th>
                                <th>Customer Name</th>
                                <th>Transaction Date</th>
                                <th>Transaction Time</th>
                                <th>Transaction Type</th>
                                <th>Customer Email</th>
                                <th>Customer Phone</th>
                                <th>Invite Sent?</th>
                                <th>Invite Method</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.rows.map((row) =>
                                <tr>
                                    <td>{row.cust_num}</td>
                                    <td>{row.cust_fname}</td>
                                    <td>{row.trans_date}</td>
                                    <td>{row.trans_time}</td>
                                    <td>{row.trans_type}</td>
                                    <td>{row.cust_email}</td>
                                    <td>{row.cust_phone}</td>
                                    <td>{row.invite_sent ? 'true' : 'false'}</td>
                                    <td>{row.invite_method}</td>
                                </tr>
                            )}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </Grid>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))

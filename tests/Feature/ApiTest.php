<?php
/**
 * Created by PhpStorm.
 * User: Jason
 * Date: 12/9/2018
 * Time: 11:38 PM
 */

namespace Tests\Feature;

use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;

class ApiTest extends TestCase
{
    /**
     * Should return March 5, 2018
     *
     * @return void
     */
    public function testDateRoute()
    {
        $today = '2018-03-05';

        $response = $this->get('/api/date');

        $response->assertSuccessful()
                 ->assertJson(['date' => $today]);
    }

    /**
     * Should accept a file of type CSV
     *
     * @return void
     */
    public function testCsvUpload()
    {

        $file = new UploadedFile('storage/app/test1.csv', 'test1.csv', 'text/csv');

        $response = $this->post('/api/upload', ['file' => $file]);

        $expected_data = [
                            [
                                "trans_type"=> "service",
                                "trans_date"=> "2018-01-02",
                                "trans_time"=> "21:00:00",
                                "cust_num"=> "10019",
                                "cust_fname"=> "Jerri Sanders",
                                "cust_email"=> "",
                                "cust_phone"=> "5526691245",
                                "invite_sent"=> false,
                                "invite_method"=> ""
                            ],
                            [
                                "trans_type"=> "sales",
                                "trans_date"=> "2018-02-27",
                                "trans_time"=> "20:00:00",
                                "cust_num"=> "10018",
                                "cust_fname"=> "Jacob",
                                "cust_email"=> "",
                                "cust_phone"=> "",
                                "invite_sent"=> false,
                                "invite_method"=> ""
                            ],
                            [
                                "trans_type"=> "sales",
                                "trans_date"=> "2018-03-01",
                                "trans_time"=> "11:20:00",
                                "cust_num"=> "10012",
                                "cust_fname"=> "Bobby",
                                "cust_email"=> "bob@gmail.com",
                                "cust_phone"=> "123-122-1223",
                                "invite_sent"=> true,
                                "invite_method"=> "email"
                            ],
                            [
                                "trans_type"=> "sales",
                                "trans_date"=> "2018-03-01",
                                "trans_time"=> "13:00:00",
                                "cust_num"=> "10012",
                                "cust_fname"=> "Bob",
                                "cust_email"=> "bob1@gmail.com",
                                "cust_phone"=> "123-123-1234",
                                "invite_sent"=> false,
                                "invite_method"=> ""
                            ],
                            [
                                "trans_type"=> "service",
                                "trans_date"=> "2018-03-01",
                                "trans_time"=> "14:00:00",
                                "cust_num"=> "10013",
                                "cust_fname"=> "Bob",
                                "cust_email"=> "bob2@gmail.com",
                                "cust_phone"=> "123-234-2345",
                                "invite_sent"=> true,
                                "invite_method"=> "email"
                            ],
                            [
                                "trans_type"=> "sales",
                                "trans_date"=> "2018-03-01",
                                "trans_time"=> "16:00:00",
                                "cust_num"=> "10014",
                                "cust_fname"=> "Robert",
                                "cust_email"=> "",
                                "cust_phone"=> "123568556",
                                "invite_sent"=> false,
                                "invite_method"=> ""
                            ],
                            [
                                "trans_type"=> "service",
                                "trans_date"=> "2018-03-01",
                                "trans_time"=> "19:00:00",
                                "cust_num"=> "10017",
                                "cust_fname"=> "Jane",
                                "cust_email"=> "jane@gmail",
                                "cust_phone"=> "111-222-3333",
                                "invite_sent"=> false,
                                "invite_method"=> ""
                            ],
                            [
                                "trans_type"=> "sales",
                                "trans_date"=> "2018-03-02",
                                "trans_time"=> "17:30:00",
                                "cust_num"=> "10015",
                                "cust_fname"=> "Jill",
                                "cust_email"=> "jill@gmail,com",
                                "cust_phone"=> "",
                                "invite_sent"=> false,
                                "invite_method"=> ""
                            ],
                            [
                                "trans_type"=> "service",
                                "trans_date"=> "2018-03-02",
                                "trans_time"=> "18:00:00",
                                "cust_num"=> "10016",
                                "cust_fname"=> "Jillian",
                                "cust_email"=> "",
                                "cust_phone"=> "224-225-2228",
                                "invite_sent"=> true,
                                "invite_method"=> "phone"
                            ],
                            [
                                "trans_type"=> "service",
                                "trans_date"=> "2018-03-04",
                                "trans_time"=> "14:00:00",
                                "cust_num"=> "10020",
                                "cust_fname"=> "Bobby",
                                "cust_email"=> "bob@gmail.com",
                                "cust_phone"=> "123-122-1223",
                                "invite_sent"=> true,
                                "invite_method"=> "email"
                            ]
                        ];

        $response->assertStatus(200)
                 ->assertJson(['transactions' => $expected_data]);
    }

    /**
     * Should reject a CSV file that lacks a trans_type
     *
     * @return void
     */
    public function testTransTypeValidation()
    {
        $file = new UploadedFile('storage/app/test2.csv', 'test2.csv', 'text/csv');

        $response = $this->post('/api/upload', ['file' => $file]);

        $response->assertStatus(400)
                 ->assertSeeText('File must include trans_type column');
    }

    /**
     * Should reject a CSV file that lacks a trans_date
     *
     * @return void
     */
    public function testTransDateValidation()
    {
        $file = new UploadedFile('storage/app/test3.csv', 'test3.csv', 'text/csv');

        $response = $this->post('/api/upload', ['file' => $file]);

        $response->assertStatus(400)
            ->assertSeeText('File must include trans_date column');
    }

    /**
     * Should reject a CSV file that lacks a cust_num
     *
     * @return void
     */
    public function testTransTimeValidation()
    {
        $file = new UploadedFile('storage/app/test4.csv', 'test4.csv', 'text/csv');

        $response = $this->post('/api/upload', ['file' => $file]);

        $response->assertStatus(400)
            ->assertSeeText('File must include trans_time column');
    }

    /**
     * Should reject a CSV file that lacks both a cust_num
     *
     * @return void
     */
    public function testCustNumValidation()
    {
        $file = new UploadedFile('storage/app/test5.csv', 'test5.csv', 'text/csv');

        $response = $this->post('/api/upload', ['file' => $file]);

        $response->assertStatus(400)
            ->assertSeeText('File must include cust_num column');
    }

    /**
     * Should reject a CSV file that lacks a cust_email and a cust_phone
     *
     * @return void
     */
    public function testCustEmailAndCustPhoneValidation()
    {
        $file = new UploadedFile('storage/app/test6.csv', 'test6.csv', 'text/csv');

        $response = $this->post('/api/upload', ['file' => $file]);

        $response->assertStatus(400)
            ->assertSeeText('File must include cust_email and/or cust_phone column');
    }

    /**
     * Should reject a file that's not CSV
     *
     * @return void
     */
    public function testOtherFileUpload()
    {
        $file = UploadedFile::fake()->create('fake.123');

        $response = $this->post('/api/upload', ['file' => $file]);

        $response->assertStatus(400)
                 ->assertSeeText('File must be CSV');
    }
}
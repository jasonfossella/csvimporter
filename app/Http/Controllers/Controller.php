<?php

namespace App\Http\Controllers;

use function GuzzleHttp\Psr7\str;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;
use ParseCsv\Csv;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $today;

    public function __construct()
    {
        $this->today = '2018-03-05';
    }

    public function date()
    {
        return response()->json(['date' => $this->today]);
    }

    public function processCsv(Request $request)
    {
        $file = $request->file('file');

        // Check if the file is CSV
        if (!in_array($file->getMimeType(), ['text/plain', 'text/csv', 'application/csv'])) {
            return (new Response())->setStatusCode(400, 'File must be CSV')
                ->setContent('File must be CSV');
        }

        $location = $file->store('/uploads');

        // import the file
        $csv = new Csv();
        $csv->auto(Storage::get($location));

        // validate CSV
        if (!in_array('cust_num', $csv->titles)) {
            return (new Response())->setStatusCode(400, 'File must include cust_num column')
                ->setContent('File must include cust_num column');
        } elseif (!in_array('trans_time', $csv->titles)) {
            return (new Response())->setStatusCode(400,'File must include trans_time column')
                ->setContent('File must include trans_time column');
        } elseif (!in_array('trans_type', $csv->titles)) {
            return (new Response())->setStatusCode(400, 'File must include trans_type column')
                ->setContent('File must include trans_type column');
        } elseif (!in_array('trans_date', $csv->titles)) {
            return (new Response())->setStatusCode(400, 'File must include trans_date column')
                ->setContent('File must include trans_date column');
        } elseif (!in_array('cust_email', $csv->titles) && !in_array('cust_phone', $csv->titles)) {
            return (new Response())->setStatusCode(400, 'File must include cust_email and/or cust_phone column')
                ->setContent('File must include cust_email and/or cust_phone column');
        }

        $data_array = $csv->data;

        // add invite_sent and method to all rows
        foreach ($data_array as &$row) {
            $row['invite_sent'] = false;
            $row['invite_method'] = '';
        }

        // sort by date and time
        usort($data_array, function($a, $b) {
            if ($a['trans_date'] > $b ['trans_date']) {
                return 1;
            } elseif ($a['trans_date'] < $b ['trans_date']) {
                return -1;
            } else {
                if ($a['trans_time'] > $b ['trans_time']) {
                    return 1;
                } elseif ($a['trans_time'] < $b ['trans_time']) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        // validate the phone numbers and emails
        $cust_nums = [];
        foreach ($data_array as &$row) {
            if (!in_array($row['cust_num'], $cust_nums)) {
                if (strtotime($this->today) - (60 * 60 * 24 * 7) < strtotime($row['trans_date'] . " " . $row['trans_time'])) {
                    if (preg_match('/^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/', $row['cust_phone'])) {
                        $row['invite_sent'] = true;
                        $row['invite_method'] = 'phone';
                        array_push($cust_nums, $row['cust_num']);
                    } elseif (filter_var($row['cust_email'], FILTER_VALIDATE_EMAIL)) {
                        $row['invite_sent'] = true;
                        $row['invite_method'] = 'email';
                        array_push($cust_nums, $row['cust_num']);
                    }
                }
            }
        }

        return response()->json(['transactions' => $data_array]);
    }
}
